<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>CESUR Desarrollo Web Entorno Servidor</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark mb-4">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Paises del Mundo</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav me-auto mb-2 mb-md-0">
      </ul>
    </div>
  </div>
</nav>
<?php
$db = new mysqli('localhost', 'root', '', 'paises');
$resultado = $db->query('SELECT * FROM paises');
$paises = $resultado->fetch_all(MYSQLI_ASSOC);
?>
<div class="container">
  <div class="row">
    <div class="col">
      <h1>Paises del Mundo</h1>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Continente</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach ($paises as $pais) : ?>
          <tr>
            <th scope="row"><?=$pais["id"]?></th>
            <td><?=$pais["nombre"]?></td>
            <td><?=$pais["continente"]?></td>
          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</body>
</html>
